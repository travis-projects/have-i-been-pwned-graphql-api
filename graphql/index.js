const { ApolloServer } = require('apollo-server-azure-functions')
const { importSchema } = require('graphql-import')
const axios = require('./utils/axios')

const resolvers = require('./resolvers')
const typeDefs = importSchema('graphql/schema.graphql')

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: ({ req, context }) => ({
    axios,
    req,
    context
  }),
  playground: true,
  introspection: true
})

module.exports = server.createHandler({
  cors: {
    origin: '*',
    credentials: true, 
  }
})
