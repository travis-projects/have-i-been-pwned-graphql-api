const axios = require('axios')

const instance = axios.create({
  baseURL: 'https://haveibeenpwned.com/api/v2/'
})

instance.defaults.headers.common['User-Agent'] = 'have-i-been-pawned-Graphql-API'

module.exports = instance