module.exports = {
  async breachedAccount (parent, { email, includeUnverified = false, domain }, { axios }, info) {
    try {
      const breaches = await axios.get(`breachedaccount/${email}`, {
        params: {
          includeUnverified,
          domain
        }
      }).then(({ data }) => data).catch(({ data }) => data)
      if (!breaches) {
        throw new Error(`Great, there is no breaches for that email address!`)
      }
      return breaches
    } catch (error) {
      throw new Error(error.message)
    }
  },
  async breaches (parent, { domain }, { axios }, info) {
    try {
      const breaches = await axios.get(`breaches`, { params: { domain } }).then(({ data }) => data).catch(({ data }) => data)
      if (!breaches.length) {
        throw new Error(`Great, there is no breaches for that domain!`)
      }
      return breaches
    } catch (error) {
      throw new Error(error.message)
    }
  },
  async breach (parent, { name }, { axios }, info) {
    try {
      const breaches = await axios.get(`breach/${name}`).then(({ data }) => data).catch(({ data }) => data)
      if (!breaches) {
        throw new Error(`Great, there is no breaches for that name!`)
      }
      return breaches
    } catch (error) {
      throw new Error(error.message)
    }
  }
}