const axios = require('axios')
const sha1 = require('simple-sha1')
module.exports = {
  async pwnedPasswords (parent, { password }, ctx, info) {
    try {
      const hashedPassword = await sha1.sync(password)
      const hashRange = hashedPassword.slice(0, 5)
      const pwnedPasswords = await axios.get(`https://api.pwnedpasswords.com/range/${hashRange}`).then(({ data }) => data).catch(({ data }) => data)
      if (!pwnedPasswords) {
        throw new Error(`Great, there is no reslts for that password!`)
      }
      return pwnedPasswords.split(`\r\n`)
    } catch (error) {
      throw new Error(error.message)
    }
  }
}