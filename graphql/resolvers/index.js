const breach = require('./breach')
const paste = require('./paste')
const passwords = require('./passwords')

module.exports = {
  Query: {
    hello: (_, args) => `Hello ${args.name || 'World'}!`,
    ...breach,
    ...paste,
    ...passwords
  }
}