module.exports = {
  async pasteAccount (parent, { email }, { axios }, info) {
    try {
      const pastes = await axios.get(`pasteaccount/${email}`).then(({ data }) => data).catch(({ data }) => data)
      if (!pastes) {
        throw new Error(`Great, there is no pastes for that email!`)
      }
      return pastes
    } catch (error) {
      throw new Error(error.message)
    }
  }
}